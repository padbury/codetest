using CodeTest.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace CodeTests
{
    [TestClass]
    public class PricingTests
    {

        [TestMethod]
        public void IsEmptyBasketEqual0()
        {
            var service = new PricingService();
            var items = new List<string>();
            Assert.AreEqual(service.Calculate(items), 0);
        }

        [TestMethod]
        public void Is_Price_Of_A_Equal_50()
        {
            var service = new PricingService();
            var items = new List<string>
            {
                "A"
            };
            Assert.AreEqual(service.Calculate(items), 50);
        }

        [TestMethod]
        public void Is_Price_Of_A_and_B_Equal_80()
        {
            var service = new PricingService();
            var items = new List<string>
            {
                "A","B"
            };
            Assert.AreEqual(service.Calculate(items), 80);
        }

        [TestMethod]
        public void Is_Price_Of_C_and_D_and_B__and_A_Equal_115()
        {
            var service = new PricingService();
            var items = new List<string>
            {
                "C","D","B","A"
            };
            Assert.AreEqual(service.Calculate(items), 115);
        }

        [TestMethod]
        public void Is_Price_Of_A_and_A_Equal_100()
        {
            var service = new PricingService();
            var items = new List<string>
            {
                "A","A"
            };
            Assert.AreEqual(service.Calculate(items), 100);
        }

        [TestMethod]
        public void Is_Price_Of_A_and_A_and_A_Equal_130()
        {
            var service = new PricingService();
            var items = new List<string>
            {
                "A","A","A"
            };
            Assert.AreEqual(service.Calculate(items), 130);
        }

        [TestMethod]
        public void Is_Price_Of_A_and_A_and_A_and_B_and_B_Equal_175()
        {
            var service = new PricingService();
            var items = new List<string>
            {
                "A","A","A","B","B"
            };
            Assert.AreEqual(service.Calculate(items), 175);
        }

    }
}
