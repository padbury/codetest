## Note

Requires .Net Core 3.1
(dotnet watch -p "project path here" run)
Requires NodeJs/NPM for clientside compilation
(npm install then npm run build)
