﻿using CodeTest.Globalisation;
using System;

namespace CodeTest.Extensions
{
    public static class DecimalExtensions
    {
        public static string AsMoney(this decimal val, string cultureCode)
        {
            // otherwise, format the value in the currency/culture requested and do a bit of bankers rounding
            return String.Format(Localisation.FindCultureByCurrencyCode(cultureCode), "{0:C}", Math.Round(val, 2, MidpointRounding.AwayFromZero));
        }

   
    }
}
