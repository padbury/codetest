﻿namespace CodeTest.Models
{
    public class Price
    {
        public decimal ListPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal SellingPrice { get { return ListPrice - Discount; } }
    }
}
