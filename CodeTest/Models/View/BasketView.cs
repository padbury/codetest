﻿using System.ComponentModel.DataAnnotations;

namespace CodeTest.Models.View
{
    public class BasketView
    {
        [Required]
        public string BasketItems { get; set; }
        public Price Total { get; set; }
        public string Message { get; set; }
    }
}
