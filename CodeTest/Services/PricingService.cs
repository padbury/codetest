﻿using CodeTest.Models;
using CodeTest.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CodeTest.Services
{
    public class PricingService : IPricingService
    {
        private readonly Dictionary<string, decimal> _pricingTable;
        private readonly Dictionary<string, decimal> _offers;
        public PricingService()
        {
            // You'd probably store the prices in a database but that's a bit OTT for a quick demo so I'm storing it as a dictionary lookup
            _pricingTable = new Dictionary<string, decimal>
            {
                { "A", 50 },
                { "B", 30 },
                { "C", 20 },
                { "D", 15 }
            };
            _offers = new Dictionary<string, decimal>{
                { "AAA",130 },
                { "BB", 45 }
            };
        }

        /// <summary>
        /// Calculate the price of a list of skus
        /// </summary>
        /// <param name="skus"></param>
        /// <returns></returns>
        public Price Calculate(List<string> skus)
        {
            decimal sum = 0;
            foreach(var item in skus)
            {
                decimal val = 0;
                
                if (_pricingTable.TryGetValue(item, out val)) // check that the item exists, if it does, grab the price and add it to the total
                {
                    sum = sum + val;
                }
            }

            // calculate the discount to apply to the order.  
            decimal discount = CalculateDiscount(skus);

            return new Price { ListPrice = sum, Discount = discount };
        }

        /// <summary>
        /// Take a series of skus and determine the total discount to apply to the basket. This could have been broken down
        /// further to take the loop out the function but I chose to keep it as a single function for brevity
        /// (Yes I know that this doesn't need to be public but it can be tested as public (even though I'm not)
        /// </summary>
        /// <param name="skus"></param>
        /// <returns></returns>
        public decimal CalculateDiscount(List<string> skus)
        {
            decimal discount = 0;
            foreach (var offer in _offers)
            {
                // Grab the offers string and split them into their codes. 
                var requiredCodes = offer.Key.ToCharArray().Select(c => c.ToString()).ToList();

                // Find the total list price for the basket
                decimal listPrice = 0;
                foreach (var code in requiredCodes)
                {
                    decimal val = 0;
                    if (_pricingTable.TryGetValue(code, out val)) // if the product exists, add to the total
                    {
                        listPrice += val;
                    }
                }
                
                /* 
                 * Now cycle through each product code and compare it to the list of discounts.
                The idea here is to have a discount code such as AAA and apply a single basket item to each item in the discount.
                A one to one mapping. When a code matches a code inside the discount code, it's popped from the discount code (so it can't 
                be applied to multiple discount codes). If the discount code is completely popped, the it's a full match and the discount is applied.
                This allows the codes to be in any order and still get applied. I've explained this badly I think. Short version is that it works.
                */
        
                foreach (var item in skus)
                {
                    for (var x = 0; x < requiredCodes.Count; x++)
                    {
                        // if the code exists the offer, remove the item from the discount code
                        if (requiredCodes[x] == item)
                        {
                            requiredCodes.RemoveAt(x);
                            break;
                        }
                    }
                    if (requiredCodes.Count == 0) // if the discount code has no remaining products in then it's a full match and can be applied to the order
                    {
                        //apply discount
                        discount += listPrice - offer.Value;
                        // Renew offer so that the discount can be used when a basket has multiple discounts applicable on the same offer (e.g. a basket with 2 lots of AAA)
                        requiredCodes = offer.Key.ToCharArray().Select(c => c.ToString()).ToList();
                    }
                }

            }
            return discount;
        }

    }
}
