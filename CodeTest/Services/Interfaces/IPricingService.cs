﻿using CodeTest.Models;
using System.Collections.Generic;

namespace CodeTest.Services.Interfaces
{
    public interface IPricingService
    {
        Price Calculate(List<string> skus);
        decimal CalculateDiscount(List<string> skus);
    }
}
