﻿const path = require('path');
var webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env) => {
    return {

        entry: {
            app: './client/app.js'
        },
        output: {
            path: path.join(__dirname, 'wwwroot/dist'),
            filename: '[name].js',
            publicPath: '/dist/'
        },
        module: {
            rules: [{
                test: /\.(js|jsx)$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            exclude: /node_modules/,
                            presets: ['es2015'],
                            plugins: ['transform-class-properties']
                        }
                    }
                ]
            },
            {
                test: /\.s?css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.(svg|eot|woff|woff2|ttf)$/,
                use: ['file-loader']
            }
            ]
        },
        plugins: [
            new MiniCssExtractPlugin(),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            })
        ],
        devtool: 'source-map'
    };
};