﻿using CodeTest.Models.View;
using CodeTest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace CodeTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPricingService _pricingService;
        public HomeController(IPricingService pricingService)
        {
            _pricingService = pricingService;
        }
        public IActionResult Index()
        {
            return View(new BasketView());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(BasketView basket)
        {
            if (!ModelState.IsValid)
            {
                return View(new BasketView { BasketItems = basket.BasketItems, Total = null, Message = "Please enter items into your basket" });
            }

            // Find the total for the basket items. Take into account capitalisation (or lack of) and remove spaces.
            var total = _pricingService.Calculate(basket.BasketItems.Replace(" ","").Trim().ToCharArray().Select(x=>x.ToString().ToUpperInvariant()).ToList());
            return View(new BasketView { BasketItems = basket.BasketItems, Total = total });
        }
    }
}
