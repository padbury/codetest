﻿using System.Globalization;

namespace CodeTest.Globalisation
{
    public static class Localisation
    {
        /// <summary>
        /// Find a culture for localisation (currency in this case)
        /// </summary>
        /// <param name="currencyCode"></param>
        /// <returns></returns>
        public static CultureInfo FindCultureByCurrencyCode(string currencyCode)
        {
            // Find currency culture
            foreach (CultureInfo info in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                if (new RegionInfo(info.Name).ISOCurrencySymbol.Equals(currencyCode))
                    return info;
            }
            // Default to UK if culture code doesn't exist
            return CultureInfo.CreateSpecificCulture("EN-GB");
        }
    }
}
